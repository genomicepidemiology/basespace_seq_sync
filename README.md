# README

## Dependencies

* basespace-cli (<https://developer.basespace.illumina.com/docs/content/documentation/cli/cli-overview>)

## Setup basespace user

Run the bs command. Go to the URL, log in, and accept that bs accesses your basespace account.

This process will store the user credential in "~/.basespace".

```bash

$ bs auth -c <username> --api-server https://api.euc1.sh.basespace.illumina.com

Please go to this URL to authenticate:  https://basespace.illumina.com/oauth/device?code=XXXXX

```

## Illumina setup

In order for the application to work the sequencing machine uploading to Basespace must create Run ExperimentNames that matches Project Names.

You can check if your machine is creating the correct outputs by comparing outputs from the commands below.

```bash

$ bs -c <username> list runs
+------------------------------------+-----------+------------------------------------+-----------+
|                Name                |    Id     |           ExperimentName           |  Status   |
+------------------------------------+-----------+------------------------------------+-----------+
| 125748_M48572_1245_000000000-DLGKI | 102365487  | TestSyncRun100                    | Complete  |
+------------------------------------+-----------+------------------------------------+-----------+

$ bs -c <username> list projects
+-----------------+-----------+-------------+
|      Name       |    Id     |  TotalSize  |
+-----------------+-----------+-------------+
| TestSyncRun100  | 25484653  | 25416478524 |
+-----------------+-----------+-------------+

```

In the example output above it can be seen that **Name** matches **ExperimentName** and therefore the machine is creating the correct output.

**Note**: It is **not** Name that has to match Name.

## Setup Run database and File database

The application stores information on runs and projects from basespace in a "Run database" and information on downloaded files in a "File database". These are two text files stored physically. The location of the file is provided with the flags "--rundb" and "--filedb". At the first run these will not exists and be created by the application. In the following runs the application will use the existing files. If the application needs to run in another location the databases can be moved with the application if the synchronisation shouldn't start over but resume the synchronisation from previous runs.

**Note**: If you create a folder named "data" in the application directory, it will be ignored by git.

## Ignore (--ignore)

The application can ignore specified runs and projects found in Basespace. This is done by a tab seperated text file.

Format of file is one entry per line. An entry consists of: RunID \<tab\> ProjectID. The project id is expected to relate to the run id. The project id can be missing. If the run id is missing the id should be replaced with 'NA'.

### Example

*ignore_runs.txt*

```

NA 122754608
29263453 23428354
48756521

```

**Note:** Git will ignore any file named "ignore_runs.txt"

## Incorrectly named Runs

If the name of a run in basespace does not correspond to the name of the corresponding project, the relation needs to be specified. Input is a text file. One entry on each line. Format: run ExperimentName<tab>project name

**Important**: in the file you need to specify run ExperimentName not run Name.

### Example

*run\_name\_translation.txt*

```

111512 M01234 0100 000000000-DG334 Run300
113202 M01254 0101 000000000-EG9FE Run503
114213 M05455 0122 000000000-GNWHE Run201

```

**Note:** Git will ignore any file named "run\_name\_translation.txt"

## Incorrectly named Projects

If the name of a proect in basespace does not correspond to the name of the corresponding run, the relation needs to be specified. Input is a text file. One entry on each line. Format: project name<tab>run ExperimentName

**Note:** Git will ignore any file named "prj\_name\_translation.txt"

## Usage

```bash

usage: basespace_seq_sync.py [-h] [--userdir PATH] --rundb JSON --filedb JSON [--dry] --output PATH [--div_output {year,month,date}]
                             [--ignore TXT] [--run_translation TXT_FILE] --log TXT_FILE [--loglevel LOGLEVEL] [--download_log TXT_FILE]
                             [--bs_path PATH]

optional arguments:
  -h, --help            show this help message and exit
  --userdir PATH        User directory for basespace. Default: ~/.basespace/
  --rundb JSON          JSON file containing run database. No file is needed, if no database has been created.
  --filedb JSON         JSON file containing file database. No file is needed, if no database has been created.
  --dry                 Dry run. Will list all missing runs and projects but will not download anything.
  --output PATH         Projects/Runs will be downloaded to this folder.
  --div_output {year,month,date}
                        If set, folders named by year, month or date will be created in the output folder and downloaded Projects/Runs
                        will be downloaded to their respective year/month/date folder, decided by the creation date from the project
                        json.
  --ignore TXT          List of runs and/or projects to ignore in basespace. Format of file is one entry per line. An entry consists of:
                        RunID <tab> ProjectID. The project id is expected to relate to the run id. The project id can be missing. If the
                        run id is missing the id should be replaced with 'NA'.
  --run_translation TXT_FILE
                        If the name of the run does not correspond to the name of the corresponding project, the relation needs to be
                        specified. Input is a text file. One entry on each line. Format: run experiment name<tab>project name
  --prj_translation TXT_FILE
                        If the name of the project does not correspond to the name of the corresponding run, the relation needs to be specified. Input is a text file. One entry on each line. Format: project name<tab>run experiment name
  --log TXT_FILE        Path to where log should be written (appended)
  --loglevel LOGLEVEL   Define how much is written to the log file, by specifiyng the minimum severity of messages to print.
  --download_log TXT_FILE
                        Path to where download log should be written (appended). The download log consists of paths to the files
                        downloaded, with one path on each line.
  --bs_path PATH        Path for Basespace CLI application.

```

## Execution example

```bash

python basespace_seq_sync.py --rundb data/rundb.json --filedb data/filedb.json --output /path/to/output_dir/ --ignore ignore_runs.txt --run_translation run_name_translation.txt --log log.txt --loglevel INFO --download_log dl_log.txt --div_output year --userdir ~/.basespace

```

## Testing code

Test has been written as doctests in mardown documents with names corresponding to the python files which they test.

### Configuration before test

In order for all test to succeed you need to configure a basespace user, project, and run.

In the files:

```text

lib/basespace.md
lib/sync_mixin.md
lib/userdata.md

```

You need to set the following variables:

```python

# This should match the filename in the folder .basespace/
# Example: ./basespace/user.cfg
BS_USER = "user"
# Location of and name of .basespace/ folder.
# '.basespace/' is just the default name.
USERDIR = "/my/dir/.basespace/"
# A valid and existing project ID.
# List all project IDs and names with 'bs list project'
BS_PRJ_ID = "105357305"
# A valid and existing project name.
BS_PRJ_NAME = "Run146"
# A valid and existing run ID.
BS_RUN_ID = "212979800"

```

### Run tests

Test in general is run by simply:

```python

$ python -m doctest *.md
# The app will display some warnings.
# This is ok, they are from the app not the tests
$ python -m doctest lib/*.md
# Expect warnings like this:
WARNING:root:Download path (/Users/rolf/workcloud/Scripts-FVST/basespace_seq_sync/test/temp/not_real) does not exist or is not a directory.
ERROR: *** BASESPACE.COMMON.AUTHENTICATION_FAILURE: Please ensure that valid credentials are being provided for this API call (JWT authentication is not supported with this request) ***
WARNING:root:Failed to download project: ricks_dna from user: rick
rsync: link_stat "/This/path/does/not/existimsureofit_sehfee" failed: No such file or directory (2)
rsync error: some files could not be transferred (code 23) at /AppleInternal/Library/BuildRoots/66382bca-8bca-11ec-aade-6613bcf0e2ee/Library/Caches/com.apple.xbs/Sources/rsync/rsync/main.c(996) [sender=2.6.9]
CRITICAL:root:!! ERROR: When attempting to move downloaded directory
Downloaded directory: /This/path/does/not/existimsureofit_sehfee
Move location: /Users/rolf/workcloud/Scripts-FVST/basespace_seq_sync/test/temp/2018/SyncTestRun100
Downloaded directory will be removed.
# Failed test will end in something like this:
***Test Failed*** 3 failures.

```

## Known Issues

* If several runs are downloaded at the same time, all downloads will fail if one of the runs fails to download. This will cause the runs to get downloaded every time the application is executed as it is never registered as complete until the failed run is handled.
