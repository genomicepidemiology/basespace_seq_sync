#!/usr/bin/env python3

import sys
import os
import os.path
import argparse
import subprocess
import shutil
import logging

import json
import re
from collections import namedtuple

from lib.rundb import RunDB
from lib.basespace import Basespace, Project
from lib.filedb import FileDB
from lib.userdata import UserData
from lib.sync_mixin import SyncMixin
from lib.log import SyncLogMixin


def load_ignore_file(file_path):
    with open(file_path, "r") as fh:
        ignore_runs_list = []
        ignore_projects_list = []
        for line in fh:
            line = line.strip()
            if (not line or line.startswith("#")):
                continue
            run_id, *project_id = line.split("\t")
            ignore_runs_list.append(run_id)
            if (project_id):
                ignore_projects_list.append(project_id[0])
    return (tuple(ignore_runs_list), tuple(ignore_projects_list))


def load_translation(file_path):
    translation = {}
    with open(file_path, "r") as fh:
        for line in fh:
            line = line.strip()
            if (not line or line.startswith("#")):
                continue
            org_name, translated_name = line.split("\t")
            translation[org_name] = translated_name
    return translation


if __name__ == "__main__":

    #
    # Handling arguments
    #
    parser = argparse.ArgumentParser(description="")

    parser.add_argument("--userdir",
                        help=("User directory for basespace. Default: "
                              "~/.basespace/"),
                        metavar="PATH",
                        default="~/.basespace/")

    parser.add_argument("--rundb",
                        help=("JSON file containing run database. No file is "
                              "needed, if no database has been created."),
                        metavar="JSON",
                        required=True)

    parser.add_argument("--filedb",
                        help=("JSON file containing file database. No file is "
                              "needed, if no database has been created."),
                        metavar="JSON",
                        required=True)

    parser.add_argument("--dry",
                        help=("Dry run. Will list all missing runs and "
                              "projects but will not download anything."),
                        action="store_true",
                        default=False)

    parser.add_argument("--output",
                        help=("Projects/Runs will be downloaded to this "
                              "folder."),
                        metavar="PATH",
                        required=True)

    parser.add_argument("--div_output",
                        help=("If set, folders named by year, month or date"
                              " will be created in the output folder and "
                              "downloaded Projects/Runs will be downloaded to "
                              "their respective year/month/date folder, decided"
                              " by the creation date from the project json."),
                        choices=["year", "month", "date"],
                        default=None)

    parser.add_argument("--flat_dir",
                        help=("If set, will move all fastq files downloaded "
                              "from the deafult folder to the top output folder"
                              " set by the 'output' flag."),
                        action="store_true",
                        default=False)

    parser.add_argument("--ignore",
                        help=("List of runs and/or projects to ignore in "
                              "basespace. Format of file is one entry per "
                              "line. An entry consists of: RunID <tab> "
                              "ProjectID. The project id is expected to relate"
                              " to the run id. The project id can be missing. "
                              "If the run id is missing the id should be "
                              "replaced with 'NA'."),
                        metavar="TXT")

    parser.add_argument("--run_translation",
                        help=("If the name of the run does not correspond to "
                              "the name of the corresponding project, the "
                              "relation needs to be specified. Input is a "
                              "text file. One entry on each line. Format: run "
                              "experiment name<tab>project name"),
                        metavar="TXT_FILE")

    parser.add_argument("--prj_translation",
                        help=("If the name of the project does not correspond "
                              "to the name of the corresponding run, the "
                              "relation needs to be specified. Input is a "
                              "text file. One entry on each line. Format: "
                              "project name<tab>run experiment name"),
                        metavar="TXT_FILE")

    parser.add_argument("--log",
                        help=("Path to where log should be written (appended)"),
                        required=True,
                        metavar="TXT_FILE")

    parser.add_argument("--loglevel",
                        help=("Define how much is written to the log file, by "
                              "specifiyng the minimum severity of messages to "
                              "print."),
                        metavar='LOGLEVEL',
                        choices=["DEBUG", "INFO", "WARNING"],
                        default="INFO")

    parser.add_argument("--download_log",
                        help=("Path to where download log should be written "
                              "(appended). The download log consists of paths "
                              "to the files downloaded, with one path on each "
                              "line."),
                        default=None,
                        metavar="TXT_FILE")

    parser.add_argument("--bs_path",
                        help=("Path for Basespace CLI application."),
                        default="bs",
                        metavar="PATH")

    args = parser.parse_args()

    SyncLogMixin.init_log(args.loglevel, args.log,
                          log_start_msg="Download Basespace runs started")

    prgs = {"bs": args.bs_path}

    args.userdir = os.path.expanduser(args.userdir)

    # Load ignore file
    if (args.ignore):
        ignore_runs, ignore_projects = load_ignore_file(args.ignore)

    # Load run db
    rundb = RunDB(args.rundb)

    basespace = Basespace(user_dir=args.userdir, prg_bs=prgs["bs"])

    # load run translation file
    if (args.run_translation):
        run_translation = load_translation(args.run_translation)
        basespace.set_run_translation(run_translation)

    # load project translation file
    if (args.prj_translation):
        prj_translation = load_translation(args.prj_translation)
        basespace.set_prj_translation(prj_translation)

    # Saves all projects and runs not found in rundb in a list of UserData
    # objects.
    users_data = []
    for user in basespace.users:
        userdat = UserData(user, basespace, rundb)
        userdat.load_missing_data(ignore_projects, ignore_runs)
        users_data.append(userdat)

    if (not args.dry):
        # Load file db
        filedb = FileDB(args.filedb)

        logging.info("Downloading...")
        basespace.set_download_folder(args.output)

        for userdat in users_data:
            logging.info("    User: {}".format(userdat.user.name))
            SyncMixin.download_missing_data(userdat, filedb, log=True,
                                            download_log=args.download_log,
                                            divide=args.div_output,
                                            flat_dir_struct=args.flat_dir)

        filedb.save()
        rundb.save()

    sys.exit()
