#!/usr/bin/env python3

import sys
import io
import os
import os.path
import shutil
import hashlib
import tempfile

import json
import re

import argparse


class DB():
    def __init__(self, db_file):
        self.db_file = db_file
        self.restore_file = "{}.bckp".format(db_file)
        self.db_start_hash = None
        self.db_new_hash = None
        self.load_db(db_file)

    def load_db(self, db_file):
        if(os.path.exists(db_file)):
            with open(db_file, "rb") as db_fh:
                self.db_start_hash = DB.sha256(db_fh)

            with open(db_file, "r") as fh:
                self.db = json.load(fh)
        else:
            self.db = {}

    def save(self):
        """ If the database exists as a json file it is compared to the current
            state of the database using a sha256 checksum.
            If the checksums are different, the json database currently stored
            in the object is renamed/moved to a restoration file and the
            current version of the database is written to the database file.
        """

        temp_descriptor, tempdbfile = tempfile.mkstemp()

        with open(temp_descriptor, mode="w") as temp_fh:
            json.dump(self.db, temp_fh)

        with open(tempdbfile, "rb") as temp_fh:
            self.db_new_hash = DB.sha256(temp_fh)

        if(self.db_new_hash == self.db_start_hash):
            os.remove(tempdbfile)
            return

        if(os.path.exists(self.db_file)):
            shutil.move(self.db_file, self.restore_file)
        shutil.move(tempdbfile, self.db_file)

    def restore(self):
        """ Will restore the database to its previous state while storing the
            current state in the restore file.
            During restore twice will therefore result in the current state.
        """
        if(os.path.exists(self.restore_file)):
            self.load_db(self.restore_file)
            self.save()

    @classmethod
    def md5(cls, fh):
        return cls._hash_bytestr_iter(
            cls._file_as_blockiter(fh), hashlib.md5())

    @classmethod
    def sha256(cls, fh):
        return cls._hash_bytestr_iter(
            cls._file_as_blockiter(fh), hashlib.sha256())

    @staticmethod
    def _hash_bytestr_iter(bytesiter, hasher, ashexstr=True):
        for block in bytesiter:
            hasher.update(block)
        return hasher.hexdigest() if ashexstr else hasher.digest()

    @staticmethod
    def _file_as_blockiter(afile, blocksize=65536):
        block = afile.read(blocksize)
        while len(block) > 0:
            yield block
            block = afile.read(blocksize)
        afile.seek(0)

    def search(self, query):
        results = []
        for key, entry in self.db.items():
            is_hit = True

            for q, v in query.items():
                entry_val = entry.get(q, None)
#DEBUG
                if(entry_val is None):
                    print("NONE entry:\n{}\n{}".format(entry, q))
                if(str(entry_val) not in v):
                    # print("Entry {} not in v: {}".format(entry_val, v))
                    # quit()
                    is_hit = False
                    break

            if(is_hit):
                results.append(entry)

        return results
