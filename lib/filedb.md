# FileDB Tests

```python

>>> from lib.filedb import FileDB, _File
>>> from lib.basespace import Project, Run
>>> import os.path
>>> import inspect
>>> filedb_py_file = inspect.getfile(FileDB)
>>> root_dir = os.path.dirname(os.path.realpath(filedb_py_file))[:-3]
>>> tmp_data_dir = ("{}/test/temp/".format(root_dir))
>>> test_data_dir = ("{}/test/data/".format(root_dir))
>>> filedb_file = "{}/test_filedb.json".format(test_data_dir)

```

## Init(name, prj_id, path)

```python

>>> filedb = FileDB(filedb_file)

```

## add_run_files(name, prj_id, path)

```python

>>> filedb.db.get("test_seq_1_R1.fq.gz", "None")
'None'
>>> run_files_path = "{}/SyncTestRun100/".format(test_data_dir)
>>> filedb.add_run_files(name="Run200", prj_id="100004", path=run_files_path)
>>> filedb.db["test_seq_1_R1.fq.gz"]["prj_name"]
'Run200'
>>> filedb.db["test_seq_1_R2.fq.gz"]["prj_name"]
'Run200'
>>> filedb.db["test_seq_2.fq.gz"]["prj_id"]
'100004'
>>> filedb.db["test_seq_2_clone.fq.gz"]["prj_id"]
'100004'
>>> filedb.db.get("ignore_non_gz.fq", "None")
'None'

```

# _File tests

## Init(filename, file_path, prj_name, prj_id, md5, sha256)

```python

>>> _file = _File(
...    filename="filnavn", file_path="/path/to/", prj_name="PRJ01",
...    prj_id="50002", md5="8cdc5300e23e897b457b8645ee2c9fb1",
...    sha256="72af2a38046ba80d3fff0e4025ed6b005b7a3567fb8a283a7b2c81c4a5862a5s")
>>> _file["filename"]
'filnavn'

```
