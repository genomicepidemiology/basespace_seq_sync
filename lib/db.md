# Test DB Class

## Setup

```python

>>> from lib.db import DB
>>> import os.path
>>> import inspect
>>> rundb_file = inspect.getfile(DB)
>>> root_dir = os.path.dirname(os.path.realpath(rundb_file))[:-3]
>>> tmp_data_dir = ("{}/test/temp/".format(root_dir))
>>> test_data_dir = ("{}/test/data/".format(root_dir))

```

## Init(db_file) - New database

```python


>>> db_file = "{}/test_db.json".format(tmp_data_dir)
>>> import os
>>> if(os.path.exists(db_file)):
...     os.remove(db_file)
>>> db = DB(db_file)
>>> db.db
{}

>>> db.db["some_key"] = "some_val"

```

## save()

Test if DB class is saved to a file.
Test if an existing file is not rewritten if the database saved is identical.
Test that a backup file is created when the original file gets updated.

```python

>>> db.save()

>>> with open(db_file, "r") as fh:
...     file_number = fh.fileno()
...     time_created_1 = os.fstat(file_number).st_mtime
>>> db2 = DB(db_file)
>>> db2.db
{'some_key': 'some_val'}
>>> db2.save()
>>> with open(db_file, "r") as fh:
...     file_number = fh.fileno()
...     time_created_2 = os.fstat(file_number).st_mtime
>>> assert(time_created_2 == time_created_1)

>>> db2.db['some_key'] = 'new_val'
>>> db2.save()
>>> with open(db_file, "r") as fh:
...     file_number = fh.fileno()
...     time_created_3 = os.fstat(file_number).st_mtime
>>> assert(time_created_3 != time_created_1)
>>> assert(os.path.exists(db2.restore_file))

```

## md5(fh) and sha256(fh)

```python

>>> test_ignore_file ="{}/test_ignore_file.txt".format(test_data_dir)
>>> with open(test_ignore_file, "rb") as db_fh:
...     sha256sum = DB.sha256(db_fh)
...     md5sum = DB.md5(db_fh)
>>> sha256sum
'538b8c803831eb004855904265f3ff4ba3a4a6152c2361f2db08c8bff2ce4bdc'
>>> md5sum
'43b4058a3a972e27d17379c1c7887f07'

```

## Tests TODO

* restore (remove?)
* search (remove?)
