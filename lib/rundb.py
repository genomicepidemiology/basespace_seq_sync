#!/usr/bin/env python3

import sys
import os
import os.path
import argparse
import subprocess
import shutil
import logging
import json
import re

from .db import DB


class RunDB(DB):
    def __init__(self, db_file):
        DB.__init__(self, db_file)
        self.name2id = {}
        for id, prj_dict in self.db.items():
            self._load_name2id(id, prj_dict["name"])

    def _load_name2id(self, id, name):
        """ Names are not unique and can potentially have several ids """
        ids = self.name2id.get(name, [])
        ids.append(id)
        self.name2id[name] = ids

    def add(self, prj, run):
        if(prj.id in self.db):
            if(run.name in self.name2id):
                self._sync_metadata(run)
            else:
                logging.critical(
                    "Run entry exist in database but cannot sync."
                    " project and run name are probably different. {} != {}"
                    .format(prj.name, run.name))
            return

        self.db[prj.id] = {"name": prj.name,
                           "run_id": run.id,
                           "run_bpname": run.bpname,
                           "run_status": run.status}
        self._load_name2id(prj.id, prj.name)

    def compare_runs(self, runs, sync=True):
        """ compare runs will return a dict of runs not found in the RunDB.
            Additionally, the method will also update found runs with missing
            metadata. Key: Run.name Val: Run
        """
        missing_runs = {}
        for run_id, run in runs.items():
            if(run.name in self.name2id and sync):
                if(len(self.name2id[run.name]) == 1 and sync):
                    self._sync_metadata(run)
                elif(sync):
                    logging.critical(
                        "! Warning: Could not sync metadata of run with id:"
                        " {} as it matches several or no run names: {}"
                        .format(run_id, self.name2id[run.name]))
            else:
                identical_named_runs = missing_runs.get(run.name, [])
                self._append_run_if_different(identical_named_runs, run)
                missing_runs[run.name] = identical_named_runs
        return missing_runs

    @staticmethod
    def _append_run_if_different(run_list, run):
        for entry in run_list:
            if(entry.id == run.id and entry.name == run.name):
                return
        run_list.append(run)

    def compare_projects(self, projects, sync=True):
        """ compare projects will return a dict of projects not found in the
            RunDB. Additionally, the method will also update entries with
            missing project ids.
        """
        missing_projects = {}
        for prj_id, prj in projects.items():
            if(prj_id in self.db):
                continue
            # If name is found, the project id is missing
            elif(prj.name in self.db and sync):
                entry = self.db[prj.name]
                self.db[prj_id] = entry
                self.name2id[prj.name] = [prj_id]
                del self.db[prj.name]
            else:
                missing_projects[prj_id] = prj
        return missing_projects

    def _sync_metadata(self, run):
        """ Assumes the matching name in RunDB is unique """
        prj_id = self.name2id[run.name][0]
        entry = self.db[prj_id]
        if("run_id" not in entry):
            entry["run_id"] = run.id
        if("run_status" not in entry):
            entry["run_status"] = run.status
        if("run_bpname" not in entry):
            entry["run_bpname"] = run.bpname

    def get_runs_by_name(self, name):
        runs = []
        ids = self.name2id.get(name, [])
        for id in ids:
            run = self.db[id]
            runs.append(run)
        return runs
