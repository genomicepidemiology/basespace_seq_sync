#!/usr/bin/env python3

import logging
import sys
import subprocess
import os
import os.path
from shutil import rmtree
from .basespace import CompletedDownload


class SyncMixin():

    def download_missing_data(userdat, filedb, log=True, download_log=None,
                              flat_dir_struct=False, divide=None):

        missing_projects = userdat.get_missing_projects()
        missing_runs = userdat.get_missing_runs()

        if (not missing_projects and log):
            logging.debug("    No projects missing.")
            return

        for p in missing_projects.values():
            if (log):
                logging.info("    Prj: {}".format(p.name))

            try:
                runs = missing_runs[p.name]
            except KeyError:
                logging.critical(
                    "!! ERROR: KeyErrors in missing run lists are often caused "
                    "by a mismatch between the run name and the project name in"
                    " basespace. If so, a run translation file has to be "
                    "provided. See --help.")
                raise

            if (len(runs) != 1):
                logging.critical("!! ERROR: Found several runs matching the "
                                 "project name {}. Matched the runs: {}."
                                 .format(download.project.name, runs))
                sys.exit(1)

            download = userdat.basespace.download_project(userdat.user, p,
                                                          run=runs[0])

            if (flat_dir_struct):
                SyncMixin.flatten_dir_struct(download, userdat)

            if (divide):
                SyncMixin.divide_by_calendar(download, divide, userdat)

            if download.success:
                filedb.add_run_files(name=download.project.name,
                                     prj_id=download.project.id,
                                     path=download.path)

                userdat.rundb.add(download.project, runs[0])
            else:
                rmtree(download.path, ignore_errors=True)

            if (download_log is not None):
                SyncMixin.write_download_log(download, download_log)

    @staticmethod
    def flatten_dir_struct(download, userdat):

        for filename, filepath in download.files.items():
            new_path = "{}/{}".format(download.path, filename)
            os.replace(filepath, new_path)
            download.files[filename] = new_path

        SyncMixin.remove_empty_dirs(download.path)

    @staticmethod
    def remove_empty_dirs(root_dir):
        dirs = list(os.walk(root_dir))[1:]
        for dir in dirs:
            if (not dir[2]):
                os.rmdir(dir[0])

    @staticmethod
    def divide_by_calendar(download, divider, userdat):
        year, month, date = download.date_created[:10].split("-")
        calendar_path = SyncMixin.get_calendar_path(year, month, date, divider)
        new_path = "{}/{}".format(userdat.basespace.download_folder,
                                  calendar_path)
        os.makedirs(new_path, exist_ok=True)
        new_download_path = "{}/{}".format(new_path, download.project.name)
        try:
            # subprocess.run(["mv", "-f", download.path, new_path], check=True)
            # DEBUG
            subprocess.run(
                ["rsync", "-a", download.path, new_path], check=True)
            rmtree(download.path, ignore_errors=True)
            download.path = new_download_path
        except subprocess.CalledProcessError:
            logging.critical(
                "!! ERROR: When attempting to move downloaded directory\n"
                f"Downloaded directory: {download.path}\n"
                f"Move location: {new_download_path}\n"
                "Downloaded directory will be removed.")
            download.success = False

    @ staticmethod
    def get_calendar_path(year, month, date, divider):
        div_path_lst = [year]

        if (divider == "month" or divider == "date"):
            div_path_lst.append(month)

        if (divider == "date"):
            div_path_lst.append(date)

        return "/".join(div_path_lst)

    @ staticmethod
    def write_download_log(download, download_log):
        """ Input:
                download: lib.basespace.CompletedDownload object
                download_log: file path

                Appends the file paths found in the CompletedDownload object
                to the download_log file. One path on each line.
        """
        with open(download_log, "a") as fh:
            for file, file_path in download.files.items():
                fh.write("{}\n".format(file_path))
