# Basespace class tests

These tests require you've set up Basespace users in the directory "~/.basespace" and that the "bs" application is found in your PATH.

## IMPORTANT dependencies

Several tests will only work if the variables in this section is set to a valid user owning the specified project.

```python

>>> import os.path

>>> BS_USER = "kale"
>>> BS_PRJ_NAME = "Run146"
>>> BS_PRJ_ID = "105357305"
>>> USERDIR = os.path.expanduser("~/.basespace/")
>>> BS_RUN_ID = "212979800"

```

## Setup

```python

>>> from lib.basespace import Basespace, Run, Project, CompletedDownload, User
>>> import inspect
>>> basespace_file = inspect.getfile(Basespace)
>>> root_dir = os.path.dirname(os.path.realpath(basespace_file))[:-3]
>>> tmp_data_dir = ("{}/test/temp/".format(root_dir))
>>> test_data_dir = ("{}/test/data/".format(root_dir))
>>> test_seqdata_dir = ("{}/test/data/SyncTestRun100/".format(root_dir))
>>> test_cfg_file = "{}/test.cfg".format(test_data_dir)

```

## Init(user_dir, prg_bs) and load_users(dir_path, extension)

```python

>>> basespace = Basespace(user_dir=USERDIR, prg_bs="bs")
>>> assert(len(basespace.users) > 0)

```

## load_users(dir_path, extension=".cfg")

Called through __init__.

```python

>>> bs_users = [u.name for u in basespace.users]
>>> assert(BS_USER in tuple(bs_users))

```

## set_run_translation(translation_dict)

```python

>>> basespace.set_run_translation({})
>>> basespace.run_translation
{}

```

## translate_run_name(run)

```python

>>> basespace.set_run_translation({ "RunTransl": "Run999" })
>>> run_transl = Run(bpname="100003_M04000_0060_000000000-ABCDE",
...                  id="1234", exp_name="RunTransl", status="Complete")
>>> run_transl = basespace.translate_run_name(run_transl)
>>> run_transl.name
'Run999'
>>> basespace.set_run_translation({})

```

## set_prj_translation(translation_dict)

```python

>>> basespace.set_prj_translation({})
>>> basespace.prj_translation
{}

```

## translate_prj_name(run)

```python

>>> basespace.set_prj_translation({ "PrjTransl": "Prj999" })
>>> prj_transl = Project(name="PrjTransl", id="4567", totalsize=700)
>>> run_transl = basespace.translate_prj_name(prj_transl)
>>> run_transl.name
'Prj999'
>>> basespace.set_prj_translation({})

```

## set_download_folder(path)

Will output a warning on successful test.

```python

>>> basespace.set_download_folder(tmp_data_dir)
>>> basespace.download_folder
... #doctest: +ELLIPSIS
'.../test/temp'
>>> basespace.set_download_folder(tmp_data_dir + "/not_real")
Traceback (most recent call last):
SystemExit: 1

```

## get_user_by_name(name)

```python

>>> real_user = basespace.get_user_by_name(BS_USER)
>>> assert(real_user.name == BS_USER)

>>> none_existing_user = basespace.get_user_by_name("Does not exist")
>>> print(none_existing_user)
None

```

## download_project(user, project, retry=3) and download_sample_sheet(user, run, out_dir)

First test produces:
ERROR: *** BASESPACE.COMMON.AUTHENTICATION_FAILURE
WARNING:root:Failed to download project: ricks_dna from user: rick

```python

>>> prjfail = Project(name="ricks_dna", id="1234", totalsize="1975623")
>>> user_fail = User(config_file=test_cfg_file, name="rick")
>>> cfail = basespace.download_project(user=user_fail, project=prjfail, retry=1)
Traceback (most recent call last):
SystemExit: 1

>>> prjdl = Project(name=BS_PRJ_NAME, id=BS_PRJ_ID, totalsize="1975623")
>>> rundl = Run(bpname="100003_M04000_0060_000000000-ABCDE",
...             id=BS_RUN_ID, exp_name="RunSample", status="Complete")
>>> cmpdl = basespace.download_project(user=real_user, project=prjdl, run=rundl,
...                                    retry=3)
>>> sample_sheet_path = ("{}/{}/SampleSheet.csv"
...                      .format(basespace.download_folder, BS_PRJ_NAME))
>>> from os.path import exists
>>> assert(exists(sample_sheet_path))

```

## get_runs(user, complete=True, running=False, stopped=False, timeout=False, ignore_ids=())

```python

>>> completed_runs = basespace.get_runs(user=real_user, complete=True)
>>> assert(len(completed_runs) > 3)
>>> completed_runs[BS_RUN_ID].status
'Complete'

>>> all_runs = basespace.get_runs(user=real_user, complete=True, running=True,
...                               stopped=True, timeout=True)
>>> assert(len(all_runs) > len(completed_runs))

>>> not_all_completed_runs = basespace.get_runs(user=real_user, complete=True,
...                                             ignore_ids=(BS_RUN_ID,))
>>> assert(len(completed_runs) > len(not_all_completed_runs))

```

## get_projects(self, user, ignore_ids=())

```python

>>> all_prjs = basespace.get_projects(real_user)
>>> assert(len(all_prjs) > 3)

>>> not_all_prjs = basespace.get_projects(real_user, (BS_PRJ_ID,))
>>> assert(len(all_prjs) > len(not_all_prjs))

```

## _read_table_line(line)

```python

>>> run_table = [
...              "+------------------------------------+-----------+------------------------------------+-----------+",
...              "|                Name                |    Id     |           ExperimentName           |  Status   |",
...              "+------------------------------------+-----------+------------------------------------+-----------+",
...              "| 170929_M04285_0057_000000000-BFLML | 36125094  | Run82                              | Complete  |",
...              "| 190726_M04285_0156_000000000-CKYNF | 190025921 | Run184                             | Timed Out |",
...              "| 210716_M04285_0256_000000000-JPMNH | 211122921 | Run282                             | Running   |",
...              "+------------------------------------+-----------+------------------------------------+-----------+"
...             ]

>>> Basespace._read_table_line(run_table[3])
['170929_M04285_0057_000000000-BFLML', '36125094', 'Run82', 'Complete']

```

## _read_table_header()

```python

>>> Basespace._read_table_header(run_table)
['Name', 'Id', 'ExperimentName', 'Status']

```

# CompletedDownload class tests

## Init(path, project)

```python

>>> prj_comp_dl = Project(BS_PRJ_NAME, BS_PRJ_ID, totalsize="1234342")
>>> out_dl_path = ("{}/{}/".format(basespace.download_folder, BS_PRJ_NAME))
>>> comp_dl = CompletedDownload(out_dl_path, prj_comp_dl)
>>> comp_dl.project.totalsize
'1234342'

```

## get_incorrectly_named_json(path: str, prj_id: str, correct_name=None) -> str

```python

>>> correct_path = f"{test_seqdata_dir}/test_tmp_json_prj.json"
>>> org_path = f"{test_seqdata_dir}SyncTestRun100_60000001.json"
>>> json_file = CompletedDownload.get_incorrectly_named_json(test_seqdata_dir, "60000001", correct_name=correct_path)
>>> assert(os.path.exists(json_file))
>>> os.replace(json_file, org_path)
>>> json_file = CompletedDownload.get_incorrectly_named_json(test_seqdata_dir, "60000001")
>>> assert(json_file == org_path)

```

## find_json_files(path)

```python

>>> json_files = tuple(CompletedDownload.find_json_files(test_data_dir))
>>> assert(len(json_files) == 2)
>>> assert(json_files[0].endswith("db.json"))
>>> assert(json_files[1].endswith("db.json"))

```

## find_gz_files(path)

```python

>>> gz_files = CompletedDownload.find_gz_files(test_seqdata_dir)
>>> assert("ignore_non_gz.fq" not in gz_files)
>>> assert("test_seq_1_R1.fq.gz" in gz_files)
>>> assert("test_seq_1_R2.fq.gz" in gz_files)
>>> assert("test_seq_2.fq.gz" in gz_files)
>>> assert("test_seq_2_clone.fq.gz" in gz_files)


```

## check_files()

```python

>>> cmpdl.check_files()

```

# Project

## Init(name, id, totalsize)

```python

>>> prj_test = Project(name="prjname", id="123456", totalsize="20000")
>>> prj_test.name
'prjname'

```

# Run

## Init(bpname, id, exp_name, status)

```python

>>> run_test = Run(bpname="bpname", id="1234", exp_name="prjname",
...                status="Complete")
>>> run_test.bpname
'bpname'

```

# User

## Init(config_file, name)

```python

>>> user_test = User(config_file=test_cfg_file, name="test")
>>> user_test.config
... #doctest: +ELLIPSIS
'...test/data//test.cfg'

```

# Clean up

```python

>>> import subprocess
>>> proc = subprocess.run(["rm", "-r", cmpdl.path], check=True)

```
