# UserData tests

## Setup

```python

>>> import os.path

>>> BS_USER = "kale"
>>> BS_PRJ_NAME = "Run146"
>>> BS_PRJ_ID = "105357305"
>>> USERDIR = os.path.expanduser("~/.basespace/")
>>> BS_RUN_ID = "212979800"  # Must be a run with the status "Complete"

>>> from lib.basespace import Basespace, Run, User
>>> from lib.rundb import RunDB
>>> import inspect
>>> basespace_file = inspect.getfile(Basespace)
>>> root_dir = os.path.dirname(os.path.realpath(basespace_file))[:-3]
>>> tmp_data_dir = ("{}/test/temp/".format(root_dir))
>>> test_data_dir = ("{}/test/data/".format(root_dir))
>>> rundb_file = "{}/test_rundb.json".format(test_data_dir)

>>> cfg_file = "{}/{}.cfg".format(USERDIR, BS_USER)
>>> user = User(config_file=cfg_file, name=BS_USER)
>>> basespace = Basespace(user_dir=USERDIR, prg_bs="bs")
>>> rundb = RunDB(rundb_file)
>>> real_user = basespace.get_user_by_name(BS_USER)

>>> from lib.userdata import UserData, ValueNotSetError

```

## Init()

```python

>>> userdata = UserData(user, basespace, rundb)
>>> assert(len(userdata.user.name) > 0)

```

## get_missing_runs()

```python

>>> userdata.get_missing_runs()
... #doctest: +ELLIPSIS
Traceback (most recent call last):
lib.userdata.ValueNotSetError: The method get_missing_runs was called...

```

## _get_missing_user_runs(ignore_runs)

The RunDB is supposed to hold information on data that has been downloaded. In this test the RunDB holds dummy information from a test rundb.json file in the test data directory. The runs from basespace is taken from the user defined in the constant "BS_USER". The user is expected to have "real" runs in basespace and these are expected to not match any runs in the rundb.json dummy file. Hence, when testing this method it is expected to return all the runs found in the BS_USER basespace account.

```python

>>> all_runs = basespace.get_runs(real_user)

>>> missing_runs = userdata._get_missing_user_runs()
>>> assert(len(all_runs) == len(missing_runs))

>>> missing_runs_ign = userdata._get_missing_user_runs(ignore_runs=(BS_RUN_ID,))
>>> assert(len(missing_runs_ign) == (len(all_runs) - 1))

```

## _get_missing_user_projects(ignore_projects, ignore_unfinished=True)

See "_get_missing_user_runs" for test explanation.

```python

>>> all_projects = basespace.get_projects(real_user)

>>> missing_projects = userdata._get_missing_user_projects(
...     ignore_unfinished=False)
>>> assert(len(all_projects) == len(missing_projects))

>>> missing_projects_ign = userdata._get_missing_user_projects(
...     ignore_projects=(BS_PRJ_ID,), ignore_unfinished=False)
>>> assert(len(missing_projects_ign) == (len(all_projects) - 1))

>>> missing_projects_finished = userdata._get_missing_user_projects(
...     ignore_unfinished=True)
>>> assert(len(all_projects) >= len(missing_projects_finished))

```

## load_missing_data(ignore_projects, ignore_runs)

```python

>>> userdata.load_missing_data()
>>> missing_prjs = userdata.get_missing_projects()
>>> missing_runs = userdata.get_missing_runs()

>>> assert(len(missing_projects) > 0)
>>> assert(len(missing_runs) > 0)

```
