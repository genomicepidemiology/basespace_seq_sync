#!/usr/bin/env python3

import sys
import os
import os.path
import argparse
import subprocess
import shutil
import logging
import json
import re

from .db import DB


class _File(dict):
    def __init__(self, filename, file_path, prj_name, prj_id, md5, sha256):
        self["filename"] = filename
        self["file_path"] = file_path
        self["prj_name"] = prj_name
        self["prj_id"] = prj_id
        self["md5"] = md5
        self["sha256"] = sha256


class FileDB(DB):

    def add_run_files(self, name, prj_id, path, ext_filter=".gz", clobber=True):
        """ Add all files found at the given path with the '.gz' extension to
            the FileDB.db dict.
        """

        gz_files = {}
        for root, dirs, files in os.walk(path):
            for file in files:
                filename, ext = os.path.splitext(file)
                file_path = "{}/{}".format(root, file)

                if(ext == ext_filter):
                    logging.debug("FileDB loading: {}".format(filename))
                    fh = open(file_path, "rb")
                    sha256 = self.sha256(fh)
                    md5 = self.md5(fh)
                    fh.close()
                    if(clobber is not True and file in self.db):
                        logging.warning(f"{file} already exists in database.")
                        continue
                    self.db[file] = _File(filename, file_path, name, prj_id,
                                          md5, sha256)
