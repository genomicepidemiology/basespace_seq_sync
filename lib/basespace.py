#!/usr/bin/env python3

import sys
import os
import os.path
import argparse
import subprocess
import shutil
import logging

import json
import re


class Basespace():
    def __init__(self, user_dir, prg_bs="bs"):
        self.users = []
        self.bs = prg_bs
        self.load_users(dir_path=user_dir)
        self.download_folder = "."
        self.run_translation = None
        self.prj_translation = None

    def get_user_by_name(self, name):
        for user in self.users:
            if (user.name == name):
                return user
        return None

    def set_run_translation(self, translation_dict):
        self.run_translation = translation_dict

    def set_prj_translation(self, translation_dict):
        self.prj_translation = translation_dict

    def set_download_folder(self, path):
        path = os.path.abspath(path)
        if (not os.path.isdir(path)):
            logging.warning("Download path ({}) does not exist or is not a"
                            " directory.".format(path))
            sys.exit(1)
        self.download_folder = path

    def download_project(self, user, project, run=None, retry=3):
        success = False
        tries = 0

        out_dir = "{}/{}".format(self.download_folder, project.name)

        self.set_user_env(user)

        cmd = [self.bs, "-q", "download", "project", "-i", project.id, "-o",
               out_dir, "--no-progress-bars"]

        while (success is False and tries < retry):
            try:
                down_proc = subprocess.run(cmd, check=True)
                completed_download = CompletedDownload(out_dir, project)
                completed_download.check_files()
                if (run is not None):
                    self.download_sample_sheet(user, run, out_dir)
                success = True
            except subprocess.CalledProcessError:
                tries = tries + 1

        if (success is False):
            logging.warning("Failed to download project: {} from user: {}"
                            .format(project.name, user.name))
            sys.exit(1)

        return completed_download

    def set_user_env(self, user):
        os.environ["BASESPACE_API_SERVER"] = user.api_server
        os.environ["BASESPACE_ACCESS_TOKEN"] = user.access_token

    def download_sample_sheet(self, user, run, out_dir):
        """ Downloads name_id.json and SampleSheet.csv to out_dir.
        """
        self.set_user_env(user)
        cmd = [self.bs, "-q", "download", "run", "-i", run.id, "-o", out_dir,
               "--extension=csv", "--no-progress-bars"]
        subprocess.run(cmd, check=True)

    def load_users(self, dir_path, extension=".cfg"):
        for root, dirs, files in os.walk(dir_path):
            for file in files:
                name, ext = os.path.splitext(file)
                file_path = "{}/{}".format(root, file)

                if (ext == ".cfg"):
                    user = User(config_file=file_path, name=name)
                    self.users.append(user)

    def get_runs(self, user, complete=True, running=False, stopped=False,
                 timeout=False, ignore_ids=None):
        """ Runs 'bs list runs' and returns a dict of runs that
            corresponds to the statuses set to true in the method call.
            Keys of the dict is run ids.
        """
        self.set_user_env(user)
        runs = {}
        comp_proc = subprocess.run([self.bs, "list", "runs"], check=True,
                                   stdout=subprocess.PIPE)
        run_table = comp_proc.stdout.decode().split("\n")

        # User without runs, return empty dict.
        if (run_table == [""]):
            return runs

        headers = self._read_table_header(run_table)
        # Skib horisontal line and headers in table
        for line in run_table[3:-2]:
            entries = tuple(self._read_table_line(line))
            run = Run(*entries)

            if (ignore_ids is not None and run.id in ignore_ids):
                continue

            if (self.run_translation):
                run = self.translate_run_name(run)

            if (run.status == "Complete" and complete):
                runs[run.id] = run
            elif (run.status == "Stopped" and stopped):
                runs[run.id] = run
            elif (run.status == "Running" and running):
                runs[run.id] = run
            elif (run.status == "Timed Out" and timeout):
                runs[run.id] = run

        return runs

    def get_projects(self, user, ignore_ids=None):
        """ Runs 'bs list projects' and returns a dict of projects.
            Keys of the dict is project ids.
        """
        self.set_user_env(user)
        projects = {}
        comp_proc = subprocess.run([self.bs, "list", "projects"], check=True,
                                   stdout=subprocess.PIPE)
        prj_table = comp_proc.stdout.decode().split("\n")

        # User without projects, return empty dict.
        if (prj_table == [""]):
            return projects

        headers = self._read_table_header(prj_table)
        # Skib horisontal line and headers in table
        for line in prj_table[3:-2]:
            entries = self._read_table_line(line)
            entries = tuple(entries)
            prj = Project(*entries)

            if (ignore_ids is not None and prj.id in ignore_ids):
                continue

            if (self.prj_translation):
                prj = self.translate_prj_name(prj)

            projects[prj.id] = prj
        return projects

    def translate_run_name(self, run):
        if (run.name in self.run_translation):
            run.name = self.run_translation[run.name]
        return run

    def translate_prj_name(self, project):
        if (project.name in self.prj_translation):
            project.name = self.prj_translation[project.name]
        return project

    @classmethod
    def _read_table_header(cls, table):
        """ Read output from 'bs list' and returns a list of headers found.
        """
        headers = []
        header_entries = cls._read_table_line(table[1])
        for header_entry in header_entries:
            headers.append(header_entry)
        return headers

    @staticmethod
    def _read_table_line(line):
        """ Read output lines from 'bs list' and returns a list of entries
            found in the line split by '|'.
        """
        # Remove table vertical sides
        entries_raw = line[1:-1]
        entries_raw = entries_raw.split("|")
        entries = [x.strip() for x in entries_raw]
        return entries


class CompletedDownload():
    def __init__(self, path, project):
        self.path = path
        self.json_file = "{}/{}_{}.json".format(path, project.name, project.id)
        if not os.path.exists(self.json_file):
            self.json_file = CompletedDownload.get_incorrectly_named_json(
                path, project.id, correct_name=self.json_file)
        with open(self.json_file, "r", encoding="utf-8") as fh:
            download_json = json.load(fh)
        self.project = project
        self.date_created = download_json["DateCreated"]
        self.files = CompletedDownload.find_gz_files(path)
        self.success = True

    @staticmethod
    def get_incorrectly_named_json(path: str, prj_id: str, correct_name=None) -> str:
        json_files = CompletedDownload.find_json_files(path)
        for json_file in json_files:
            if CompletedDownload.is_json_with_id(json_file, prj_id):
                if correct_name:
                    os.replace(json_file, correct_name)
                    return correct_name
                else:
                    return json_file

    @staticmethod
    def is_json_with_id(path: str, id: str) -> bool:
        with open(path, "r", encoding="utf-8") as fh:
            json_dict = json.load(fh)
        if json_dict.get("Id", False):
            if json_dict["Id"] == id:
                return True
        return False

    @staticmethod
    def find_json_files(path):
        json_files = []
        for dir_entry in os.scandir(path):
            entry_path, ext = os.path.splitext(dir_entry.path)
            if ext == ".json":
                json_files.append(dir_entry.path)
        return json_files

    @staticmethod
    def find_gz_files(path):
        gz_files = {}
        for root, dirs, files in os.walk(path):
            for file in files:
                name, ext = os.path.splitext(file)
                file_path = "{}/{}".format(root, file)

                if (ext == ".gz"):
                    gz_files[file] = file_path
        return gz_files

    def check_files(self):
        for filename, path in self.files.items():
            logging.debug("Checking: {}".format(filename))
            subprocess.run(["gzip", "-t", path], check=True)


class Project():
    def __init__(self, name, id, totalsize):
        self.name = name
        self.id = id
        self.totalsize = totalsize


class Run():
    def __init__(self, bpname, id, exp_name, status):
        self.bpname = bpname
        self.id = id
        self.name = exp_name
        self.status = status


class User():
    def __init__(self, config_file, name):
        self.config = config_file
        self.name = name
        self._load_config_file()

    def _load_config_file(self):
        with open(self.config, "r") as fh:
            for line in fh:
                line = line.strip()
                if (not line):
                    continue
                if (line.startswith("apiServer")):
                    self.api_server = line.split(" ")[-1]
                elif (line.startswith("accessToken")):
                    self.access_token = line.split(" ")[-1]
