# RunDB Tests

```python

>>> from lib.rundb import RunDB
>>> from lib.basespace import Project, Run
>>> import os.path
>>> import inspect
>>> rundb_py_file = inspect.getfile(RunDB)
>>> root_dir = os.path.dirname(os.path.realpath(rundb_py_file))[:-3]
>>> tmp_data_dir = ("{}/test/temp/".format(root_dir))
>>> test_data_dir = ("{}/test/data/".format(root_dir))
>>> rundb_file = "{}/test_rundb.json".format(test_data_dir)

```

## Init(db_file)

```python

>>> rundb = RunDB(rundb_file)
>>> rundb.name2id["SyncTestRun100"]
['60000001', '60000002']
>>> rundb.name2id["SyncTestRun101"]
['60000003']

```

## add(prj, run)

Projects and Runs from basespace can only be matched by project name and exp_name.

```python

# New project and run

>>> rundb.db.get("60000008", "None")
'None'
>>> prj1 = Project(name="Run106", id="60000008", totalsize="4000000")
>>> run1 = Run(bpname="10_M04000-ABCDE", id="50000011", exp_name="Run106",
...            status="Complete")
>>> rundb.add(prj1, run1)
>>> rundb.db["60000008"]["name"]
'Run106'

# Add run to existing project in rundb

>>> rundb.db["60000006"].get("run_id", "None")
'None'
>>> prj2 = Project(name="SyncTestRun104", id="60000006", totalsize="4000000")
>>> run2 = Run(bpname="10_M04000-ABCDE", id="50000014",
...            exp_name="SyncTestRun104", status="Complete")
>>> rundb.add(prj2, run2)
>>> rundb.db["60000006"]["run_status"]
'Complete'

```

## compare_runs(runs, sync=True)

```python

>>> comp_run1 = Run(bpname="10_M04000-ABCDE", id="50000016",
...                 exp_name="SyncTestRun103", status="Complete")
>>> comp_run2 = Run(bpname="10_M04000-ABCDE", id="50000018", exp_name="Run200",
...                 status="Complete")
>>> comp_runs = {
...     "50000016": comp_run1,
...     "50000018": comp_run2
... }
>>> missing_runs = rundb.compare_runs(comp_runs, sync=False)
>>> assert("SyncTestRun103" in missing_runs)
>>> assert("Run200" in missing_runs)
>>> rundb.db["60000005"].get("run_id", "None")
'None'
>>> missing_runs = rundb.compare_runs(comp_runs, sync=True)
>>> assert("SyncTestRun103" not in missing_runs)
>>> assert("Run200" in missing_runs)
>>> missing_runs["Run200"][0].name
'Run200'
>>> rundb.db["60000005"].get("run_id", "None")
'50000016'

```

## compare_projects

```python

>>> rundb.db.get("60000004", "None")
'None'
>>> comp_prj1 = Project(name="Run102", id="60000004", totalsize="4000000")
>>> comp_prj2 = Project(name="Run106", id="60000010", totalsize="4000000")
>>> comp_projects = {
...     "60000004": comp_prj1,
...     "60000010": comp_prj2
... }
>>> missing_runs = rundb.compare_projects(comp_projects, sync=False)
>>> assert("60000004" in missing_runs)
>>> assert("60000010" in missing_runs)
>>> rundb.db.get("60000004", "None")
'None'
>>> missing_runs = rundb.compare_projects(comp_projects, sync=True)
>>> assert("60000004" not in missing_runs)
>>> rundb.db["60000004"].get("run_id", "None")
'50000004'

```

## get_runs_by_name

```python

>>> runs = rundb.get_runs_by_name("SyncTestRun100")
>>> runs[0]["run_id"]
'50000001'
>>> runs2 = rundb.get_runs_by_name("Doesnt_exist")
>>> runs2
[]

```

## _append_run_if_different(run_list, run)

```python

>>> run_list = [run1, run2]
>>> RunDB._append_run_if_different(run_list, comp_run1)
>>> len(run_list)
3
>>> RunDB._append_run_if_different(run_list, run1)
>>> len(run_list)
3

```

## _sync_metadata(run)

```python

>>> rundb.db["60000007"].get("run_id", "None")
'None'
>>> run3 = Run(bpname="10_M04000-ABCDE", id="50000010",
...            exp_name="SyncTestRun105", status="Complete")
>>> rundb._sync_metadata(run3)
>>> rundb.db["60000007"].get("run_id", "None")
'50000010'

```
