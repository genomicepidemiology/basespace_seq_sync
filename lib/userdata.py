#!/usr/bin/env python3
import logging


class ValueNotSetError(Exception):
    """ Raise when an object parameter is accessed before it has been set
        correctly.
    """
    def __init__(self, message, *args):
        self.message = message
        # allow users initialize misc. arguments as any other builtin Error
        super(ValueNotSetError, self).__init__(message, *args)


class UserData():
    def __init__(self, user, basespace, rundb):
        self.user = user
        self.basespace = basespace
        self.rundb = rundb
        self._missing_projects = None
        self._missing_runs = None

    def get_missing_runs(self):
        if(self._missing_runs is None):
            raise(ValueNotSetError(
                "The method get_missing_runs was called before the method "
                "load_missing_data, that populates the parameter which "
                "get_missing_runs tries to return."))
        return self._missing_runs

    def get_missing_projects(self):
        if(self._missing_projects is None):
            raise(ValueNotSetError(
                "The method get_missing_projects was called before the method "
                "load_missing_data, that populates the parameter which "
                "get_missing_projects tries to return."))
        return self._missing_projects

    def load_missing_data(self, ignore_projects=None, ignore_runs=None):
        logging.debug("Loading user {}...".format(self.user.name))
        self._missing_projects = self._get_missing_user_projects(
            ignore_projects)
        self._missing_runs = self._get_missing_user_runs(
            ignore_runs)

    def _get_missing_user_runs(self, ignore_runs=None):
        runs = self.basespace.get_runs(user=self.user,
                                       ignore_ids=ignore_runs)
        runs_not_in_db = self.rundb.compare_runs(runs)
        for name, runs in runs_not_in_db.items():
            logging.info("Missing run named: {}".format(name))
            for run in runs:
                logging.info("\t Run id: {}".format(run.id))
        return runs_not_in_db

    def _get_missing_user_projects(self, ignore_projects=None,
                                   ignore_unfinished=True):
        projects = self.basespace.get_projects(user=self.user,
                                               ignore_ids=ignore_projects)

        projects_not_in_db = self.rundb.compare_projects(projects)
        projects_not_ready = []

        for id, prj in projects_not_in_db.items():
            if(int(prj.totalsize) > 0 or ignore_unfinished is False):
                logging.info("Missing project: {}: {}"
                             .format(prj.id, prj.name))
            else:
                logging.debug("Waiting for project to finish: {}"
                              .format(prj.name))
                projects_not_ready.append(prj)

        for prj in projects_not_ready:
            del projects_not_in_db[prj.id]

        return projects_not_in_db
