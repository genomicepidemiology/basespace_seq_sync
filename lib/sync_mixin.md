# Sync class tests

## Setup

```python

>>> import os
>>> import os.path
>>> import inspect
>>> import subprocess
>>> from lib.sync_mixin import SyncMixin
>>> from lib.basespace import Project, CompletedDownload, Basespace, User, Run
>>> from lib.userdata import UserData
>>> from lib.rundb import RunDB
>>> from lib.filedb import FileDB

>>> BS_USER = "kale"
>>> USERDIR = os.path.expanduser("~/.basespace/")
>>> BS_PRJ_ID = "105357305"
>>> BS_PRJ_NAME = "Run146"
>>> BS_RUN_ID = "212979800"

>>> sync_mixin_py_file = inspect.getfile(SyncMixin)
>>> root_dir = os.path.dirname(os.path.realpath(sync_mixin_py_file))[:-3]

>>> test_data_dir = ("{}/test/data/".format(root_dir))
>>> tmp_data_dir = ("{}/test/temp/".format(root_dir))

>>> prj_comp_dl = Project("SyncTestRun100", "60000001", totalsize="1234342")
>>> out_dl_path = ("{}/{}".format(test_data_dir, "SyncTestRun100"))

>>> comp_dl = CompletedDownload(out_dl_path, prj_comp_dl)

>>> cfg_file = "{}/{}.cfg".format(USERDIR, BS_USER)
>>> user = User(config_file=cfg_file, name=BS_USER)

>>> basespace = Basespace(user_dir=USERDIR, prg_bs="bs")
>>> basespace.set_download_folder(tmp_data_dir)

>>> rundb_file = "{}/test_rundb.json".format(test_data_dir)
>>> rundb = RunDB(rundb_file)

>>> userdata = UserData(user, basespace, rundb)

>>> filedb_file = "{}/test_filedb.json".format(test_data_dir)
>>> filedb = FileDB(filedb_file)

```

## write_download_log(download, download_log)

```python

>>> download_log_path = "{}/{}".format(tmp_data_dir, "test_download_log.txt")
>>> SyncMixin.write_download_log(download=comp_dl,
...                              download_log=download_log_path)

>>> with open(download_log_path, "r") as fh:
...     download_log_list = fh.readlines()

>>> proc = subprocess.run(["rm", download_log_path], check=True)

```

## download_missing_data(userdat, filedb, log=True, download_log=None, test=False)

```python

>>> userdata._missing_projects = {
...     BS_PRJ_ID: Project(name=BS_PRJ_NAME, id=BS_PRJ_ID, totalsize="1975623")
... }
>>> userdata._missing_runs = {
...     BS_PRJ_NAME: [
...         Run(bpname="100003_M04000_0060_000000000-ABCDE",
...             id=BS_RUN_ID, exp_name=BS_PRJ_NAME, status="Complete")
...     ]
... }

>>> SyncMixin.download_missing_data(userdata, filedb,
...                                 download_log=download_log_path,
...                                 flat_dir_struct=True)

```

## get_calendar_path(year, month, date, divider)

```python

>>> SyncMixin.get_calendar_path("2021", "12", "24", "year")
'2021'
>>> SyncMixin.get_calendar_path("2021", "12", "24", "month")
'2021/12'
>>> SyncMixin.get_calendar_path("2021", "12", "24", "date")
'2021/12/24'


```

## divide_by_calendar(download, divider, userdat)

```python

# Save value for next test
>>> org_dl_path = comp_dl.path

>>> SyncMixin.divide_by_calendar(comp_dl, "year", userdata)
>>> comp_dl.path
... #doctest: +ELLIPSIS
'...test/temp/2018/SyncTestRun100'

```

Test when the downloaded directory is attempted moved to existing directory

```python

# Setup
>>> os.makedirs(org_dl_path, exist_ok=True)

>>> comp_new_path = comp_dl.path
>>> comp_dl.path = org_dl_path
>>> SyncMixin.divide_by_calendar(comp_dl, "year", userdata)
>>> comp_dl.path
... #doctest: +ELLIPSIS
'...test/temp/2018/SyncTestRun100'

```

Test failing sync.

```python

>>> comp_new_path = comp_dl.path
>>> comp_dl.path = "/This/path/does/not/existimsureofit_sehfee"
>>> SyncMixin.divide_by_calendar(comp_dl, "year", userdata)
>>> comp_dl.path
... #doctest: +ELLIPSIS
'/This/path/does/not/existimsureofit_sehfee'

# Make sure clean up will work later
>>> comp_dl.path = comp_new_path

```

## remove_empty_dirs(root_dir)

```python

>>> tmp_empty_dirs = "{}/empty_dirs".format(tmp_data_dir)
>>> os.makedirs(tmp_empty_dirs)

>>> new_dir1 = "{}/empty_dir".format(tmp_empty_dirs)
>>> os.makedirs(new_dir1)
>>> os.path.exists(new_dir1)
True
>>> new_dir2 = "{}/non_empty_dir".format(tmp_empty_dirs)
>>> os.makedirs(new_dir2)
>>> os.path.exists(new_dir2)
True
>>> p = subprocess.run(["touch", "{}/some_file".format(new_dir2)], check=True)
>>> SyncMixin.remove_empty_dirs(tmp_empty_dirs)
>>> os.path.exists(new_dir1)
False
>>> os.path.exists(new_dir2)
True

```

## Clean up

```python

>>> proc = subprocess.run(["rm", "-rf", tmp_empty_dirs])
>>> proc = subprocess.run(["rm", download_log_path])
>>> dl_path = "{}/{}".format(tmp_data_dir, BS_PRJ_NAME)
>>> proc = subprocess.run(["rm", "-r", dl_path])
>>> proc = subprocess.run(["mv", comp_dl.path, test_data_dir])

```
