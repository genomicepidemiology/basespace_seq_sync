#!/usr/bin/env python3

import sys
import os.path
import logging


class SyncLogMixin():

    @staticmethod
    def eprint(*args, **kwargs):
        print(*args, file=sys.stderr, **kwargs)

    @staticmethod
    def init_log(loglevel, log=None, log_start_msg="Log started"):

        if(log is not None):
            log = os.path.abspath(log)
        else:
            log = None

        numeric_level = getattr(logging, loglevel.upper(), None)
        if not isinstance(numeric_level, int):
            raise ValueError("Invalid log level: {}".format(loglevel))

        logging.basicConfig(filename=log, level=numeric_level,
                            format='%(asctime)s %(levelname)s:%(message)s',
                            filemode="a")
        logging.info(log_start_msg)
