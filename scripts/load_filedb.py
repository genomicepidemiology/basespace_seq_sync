#!/usr/bin/env python3

import sys
import os
import os.path
import argparse

from lib.filedb import FileDB


DEFAULT_EXT = ".gz"


def main():

    #
    # Handling arguments
    #
    parser = argparse.ArgumentParser(description="")

    parser.add_argument("-i", "--indir",
                        help=("Directoy containing files to load"),
                        metavar="PATH",
                        required=True)

    parser.add_argument("-f", "--filedb",
                        help=("Path to fildb json file."),
                        metavar="JSON",
                        required=True)

    parser.add_argument("-n", "--prj_name",
                        help=("Project name to store (ex.: Run134)."),
                        metavar="STR",
                        required=True)

    parser.add_argument("-d", "--prj_id",
                        help=("Project id from Basespace."),
                        metavar="INT",
                        required=True)

    parser.add_argument("--extension",
                        help=("Load all files with the given extension. "
                              f"Default is: '{DEFAULT_EXT}'."),
                        metavar="EXT",
                        default=DEFAULT_EXT)

    args = parser.parse_args()

    filedb = FileDB(args.filedb)

    filedb.add_run_files(name=args.prj_name,
                         prj_id=args.prj_id,
                         path=args.indir,
                         ext_filter=args.extension,
                         clobber=False)

    filedb.save()


if __name__ == "__main__":

    sys.exit(main())
