# Tests

## Setup

```python

>>> import os.path
>>> import inspect

>>> from basespace_seq_sync import *
>>> from lib.rundb import RunDB
>>> rundb_file = inspect.getfile(RunDB)
>>> root_dir = os.path.dirname(os.path.realpath(rundb_file))[:-3]
>>> test_ignore_file = ("{}/test/data/test_ignore_file.txt".format(root_dir))

```

## load_ignore_file(file_path)

```python

>>> ign_runs, ign_prjs = load_ignore_file(test_ignore_file)
>>> ign_runs
('NA', '20283323', '18586581')
>>> ign_prjs
('123329208', '32218187')

```

## load_translation(file_path)

```python

>>> test_trans_file = ("{}/test/data/test_run_translation.txt".format(root_dir))
>>> transl = load_translation(test_trans_file)
>>> transl["Some run name"]
'Run0001'
>>> transl["Another run name"]
'Run0120'

```
